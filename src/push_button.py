import RPi.GPIO as GPIO
import time
import datetime
import requests
import json

def sendGoalToSlack(team):
    query = {'team': team}
    headers = {'Content-type':'application/json', 'Accept': 'text/plain'}
    res = requests.post(url, data=json.dumps(query), headers=headers)
        
print('Setting up pins')
GPIO.setwarnings(False)            
GPIO.setmode(GPIO.BOARD)
GPIO.setup(10, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(5, GPIO.IN, pull_up_down = GPIO.PUD_UP)

url = 'http://192.168.0.6:3000/api/v1/tables/e98d1b22-4f51-4c00-ba0a-78b2dd3f9423/goals'
query = ''

print('Ready')
while True:
    st= datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    if GPIO.input(10) == True:        
        print('{} Button Pressed for Pin 10'.format(st))
        #sendGoalToSlack('A')
    elif GPIO.input(5) == True:
        print('{} Button Pressed for Pin 5'.format(st))
        #sendGoalToSlack('B')
